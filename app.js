var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var sentiment = require('sentiment');
//var twitter = require('ntwitter');
var Twit = require('twit');


var app = express();

var port = 3000;
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var StreamConnection;

/*Initialize twitter handle*/
var twitterAccessKeys = new Twit({
	consumer_key : '6JZUwM79aSzlWGckSeloq6lOv',
	consumer_secret : 'PgZ2gSFJdI32V510gSX4y36tKJXwociCuXfVYdNq1xc6V4ghGQ',
	access_token : '28991940-mDQ9Q7A1ztxF5JBVpZfCsiQN8ixWpUCaYGdhNrQj8',
	access_token_secret : '2mQ0XgM6biq9zH6Wk3FLKrr3rta8Hx3efbwmta0w3FfRI'
});	

/*Start Server on specified port*/
server.listen(port);

/*Function to preprocess the text*/
function preProcess(tweet)
{
	/*Keep only alphabets*/
	tweet = tweet.replace(/[^a-z ]/gi,'');
	/*Convert to lowercase*/
	tweet = tweet.toLowerCase();
	/*Remove stop words (Like the..an..etc)*/
	return tweet;
}
/*This function will be called after 10 seconds of receiving tweets
*Send the results computed to the client connected*/
function sendResults(socket, countofPositiveTweets,
    					countofNegativeTweets,countofNeutralTweets,
    					listofPositiveTweets,
    					listofNegativeTweets,
    					listofNeutralTweets)
{
	/*Send results to the client*/
	console.log("datasending");

	console.log("----------positive_count-----------");
	for(var i in listofPositiveTweets)
		console.log(listofPositiveTweets[i]);

	console.log("----------negative_tweet_list-----------");
	for(var j in listofNegativeTweets)
		console.log(listofNegativeTweets[j]);

	console.log("----------neutral-----------");
	for(var k in listofNeutralTweets)
		console.log(listofNeutralTweets[k]);
	/*Send to client as a JSON Object*/
	socket.emit('hashtagResults',{
		
		positive_count: countofPositiveTweets,
		negative_count: countofNegativeTweets,
		neutral_count: countofNeutralTweets,
		positive_tweet_list: listofPositiveTweets,
		negative_tweet_list: listofNegativeTweets,
		neutral_tweet_list: listofNeutralTweets		
	});
}

/*This gets called when a browser connects to the server*/
io.sockets.on('connection', function(socket) {

		var intervalId;
	console.log("New Client Arrived!");
		
	/*Received a search query from User*/
	socket.on('searchQuery', function(data) {

		var hashTag = data.query;
		var listofNeutralTweets=[];
		var listofPositiveTweets=[];
		var listofNegativeTweets=[];
		
		var countofNeutralTweets=0;
		var countofPositiveTweets=0;
		var countofNegativeTweets=0;
		
		/*Remove Spaces from Hashtag and other special characters*/
		hashTag = hashTag.replace(/[^a-z0-9]/gi,'');
		hashTag = '#'+hashTag;
		console.log("Received from Client");
		console.log(hashTag + "is received");

		/*Check if there was a previous search done
		*If done, then the periodic sending to the client for 
		previous search must be stopped*/
		clearInterval(intervalId);

		/*Connect to Twitter and extract tweets for the 
		 * input query*/
		var stream = twitterAccessKeys.stream('statuses/filter', 
				{track : [hashTag]});
				
		StreamConnection = stream;
			console.log(stream);
			
			stream.on('tweet',function(data){
			/*Do Preprocessing on text*/
			
				//console.log("Data arrived");
				var tweet = data.text;
				var tweet_copy = data.text;				

				var preProcessedText = preProcess(tweet_copy);
				var result = sentiment(preProcessedText);
				//console.log(preProcessedText);
				if(result.score > 0)/*Positive Tweet*/
				{
					countofPositiveTweets++;
					/*For display purposes keep only 10 tweets*/
					if(countofPositiveTweets<=10)
						listofPositiveTweets.push(tweet);
				}
				else if(result.score < 0) /*Negative Tweet*/
				{
					countofNegativeTweets++;
					/*For display purposes keep only 10 tweets*/
					if(countofNegativeTweets<=10)
						listofNegativeTweets.push(tweet);
				}
				else /*Neutral Tweet*/
				{
					countofNeutralTweets++;
					/*For display purposes keep only 10 tweets*/
					if(countofNeutralTweets<=10)
						listofNeutralTweets.push(tweet);
				}								
			});
			
			/*Send results to client after 10 seconds*/
			intervalId = setInterval(function() {
				sendResults(socket, countofPositiveTweets,
				countofNegativeTweets,countofNeutralTweets,
				listofPositiveTweets,
				listofNegativeTweets,
				listofNeutralTweets);  							
			}, 10000);	


			stream.on('disconnect', function (disconnectMessage) {
  				console.log(disconnectMessage);
			});

			stream.on('error',function(errorMessage){
				socket.emit('Error',{
					msg: "The server is facing issues in connecting to Twitter. Try again later",
					code: errorMessage.statusCode
				});
				/*Stop Calling the function to emit to client
				*as this client is already disconnected*/

				clearInterval(intervalId);
				console.log(errorMessage);
				/*Disconnect with client!*/
				//socket.disconnect();
			});		
		
		});

	/*A client has disconnected*/
	socket.on('disconnect', function(data) {
		console.log("Client Has disconnected");
		/*Close Twitter Connection*/
		if(typeof this.StreamConnection!= 'undefined')
			StreamConnection.stop();
		
		/*Stop Calling the function to emit to client
		*as this client is already disconnected*/
		clearInterval(intervalId);
	});
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended : false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	//next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message : err.message,
			error : err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message : err.message,
		error : {}
	});
});

module.exports = app;
