var server_name = "http://192.168.0.21:3000/";
var socket = io.connect(server_name);
var piechart;


/*On Click of a submit button for search query*/
function onsubmit1()
{
	var searchQuery = document.getElementById("hashtagSearchBox").value;
	if(searchQuery.length == 0)
	{
		displayErrorMessage("Please Enter a search Term!!");
		return false;
	}
	
	console.log("sending!!");
	/*Check if socket is connected*/
	console.log("Status: "+ socket.connected);
	if(socket.connected == false)
		socket = io.connect({'forceNew': true});

	socket.on('connect',function(){
		console.log("Status: "+ socket.connected);
	})
	
	socket.emit('searchQuery',{
		query: searchQuery
	});	
	hideResults();
	showPreloader();
}

/*Allow Enter Key to Submit*/
$('#hashtagSearchBox').keypress(function(e){
      if(e.keyCode==13)
      	onsubmit1();
 });

/*Create HTML Output for Positive Tweets*/
function createPositiveTweetsUI(positive_tweet_list)
{
	var outputHTML="";
	console.log("-------------Positive count = " + positive_tweet_list.length);
	for(var i in positive_tweet_list)
	{
		outputHTML+='<div id="ptlist">';
		outputHTML+= '<div class="alert alert-success" role="alert">';
		outputHTML+= positive_tweet_list[i];
		console.log(positive_tweet_list[i]);
		outputHTML+= '</div>' ;/*End div alert-success*/
		outputHTML+= '</div>' ;/*End div ptlist*/
	}
	return outputHTML;
}

/*Create HTML Output for Negative Tweets*/
function createNegativeTweetsUI(negative_tweet_list)
{
	var outputHTML="";
	console.log("-------------negative_tweet_list count = " + negative_tweet_list.length);
	for(var i in negative_tweet_list)
	{
		outputHTML+='<div id="ptlist">';
		outputHTML+= '<div class="alert alert-error" role="alert">';
		outputHTML+= negative_tweet_list[i];
		console.log(negative_tweet_list[i]);
		outputHTML+= '</div>' ;/*End div alert-success*/
		outputHTML+= '</div>' ;/*End div ptlist*/
	}
	return outputHTML;
}

/*Create HTML Output for Neutral Tweets*/
function createNeutralTweetsUI(neutral_tweet_list)
{
	var outputHTML="";
	console.log("-------------neutral_tweet_list count = " + neutral_tweet_list.length);
	for(var i in neutral_tweet_list)
	{
		outputHTML+='<div id="ptlist">';
		outputHTML+= '<div class="alert alert-neutral" role="alert">';
		outputHTML+= neutral_tweet_list[i];
		console.log(neutral_tweet_list[i]);
		outputHTML+= '</div>' ;/*End div alert-success*/
		outputHTML+= '</div>' ;/*End div ptlist*/
	}
	return outputHTML;
}

/*Display all the tweets received classified as positive,
netural or negative*/
function displayTweets(positive_tweet_list,negative_tweet_list,neutral_tweet_list)
{
	var displayTweetsHTML = '<div id="displayTweets">';
	var class_of_columns; /*Determine number of columns required
	*If all 3 types of tweets are present use 3 columns and so on
	*If all 3 columns are present use col-md4 class, 
	*2 columns use col-md6
	*1 column use col-md12 (For UI)
	*/
	var count =0;
	if(positive_tweet_list.length > 0)
		count++;
	if(negative_tweet_list.length > 0)
		count++;
	if(neutral_tweet_list.length > 0)
		count++;

	if(count == 3)
		class_of_columns='<div class="col-md-4">';
	else if(count == 2)
		class_of_columns='<div class="col-md-6">';
	else if(count == 1)
		class_of_columns='<div class="col-md-12">';
	else
	{
		/*Did not find any searched hashtags on twitter
		*Display to user error*/
		displayErrorMessage("Could Not find Any Such Hashtag on Twitter. Try with \
			Another Hashtag!");
		return false; /*Error--No tweets found!*/
	}	

	if(positive_tweet_list.length > 0)
	{
		displayTweetsHTML+=class_of_columns;
		displayTweetsHTML+=createPositiveTweetsUI(positive_tweet_list);
		displayTweetsHTML+="</div>"; /*End class of columns*/
	}
	if(negative_tweet_list.length > 0)
	{
		displayTweetsHTML+=class_of_columns;
		displayTweetsHTML+=createNegativeTweetsUI(negative_tweet_list);
		displayTweetsHTML+="</div>"; /*End class of columns*/
	}
	if(neutral_tweet_list.length > 0)
	{
		displayTweetsHTML+=class_of_columns;
		displayTweetsHTML+=createNeutralTweetsUI(neutral_tweet_list);
		displayTweetsHTML+="</div>"; /*End class of columns*/
	}

	displayTweetsHTML+="</div>"; /*End class of div display Tweets*/	

	var displayWindowDiv = document.getElementById("displayWindow");
	displayWindowDiv.innerHTML = displayTweetsHTML;
}

function displayChart(positive_count,
				 	  negative_count,
				 	  neutral_count)
{
	/*Create JSON Object for chart*/
	var data=[
		{
	        value: positive_count,
	        color:"#1abc9c",
	        highlight: "#48c9b0",
	        label: "Positive Tweets"
    	},
    	{
	        value: negative_count,
	        color: "#e74c3c",
	        highlight: "#ec7063",
	        label: "Negative Tweets"
    	},
	    {
	        value: neutral_count,
	        color: "#B1B3A6",
	        highlight: "#bdc3c7",
	        label: "Neutral Tweets"
	    }
	];

	/*Create Doughnut chart*/
	var ctx = document.getElementById("piechart").getContext("2d");
	//var piechart = new Chart(ctx).Doughnut(data);

	
	if(typeof this.piechart!= 'undefined')
	{
		/*piechart.segments[0].value=positive_count;
		piechart.segments[1].value=negative_count;
		piechart.segments[2].value=neutral_count;
		piechart.update();*/
		piechart.destroy();

	}
	//else
		piechart = new Chart(ctx).Doughnut(data);
	//var displayChart = document.getElementById("displayChart");
	//displayChart.innerHTML(myDoughnutChart);

	showResults();
	hidePreloader();
}

function showPreloader()
{
	$("#preloader").show();
}
function hidePreloader()
{
	$("#preloader").hide();
}
function hideResults()
{
	$("#piechart").hide();
	$("#displayWindow").hide();
}

function showResults()
{
	$("#piechart").show();
	$("#displayWindow").show();
}

function displayErrorMessage(text)
{
	var displayErrorHTML = '<div id="displayError">';
	displayErrorHTML+='<div class="col-md-12">';
	displayErrorHTML+='<div id="ptlist">';
	displayErrorHTML+= '<div class="alert alert-error" role="alert">';
	displayErrorHTML+= text;
	displayErrorHTML+= '</div>' ;/*End div alert-success*/
	displayErrorHTML+= '</div>' ;/*End div ptlist*/
	displayErrorHTML+= '</div>' ;/*End of div column*/ 
	displayErrorHTML+="</div>"; /*End class of div display Error*/

	var displayWindowDiv = document.getElementById("displayWindow");
	displayWindowDiv.innerHTML = displayErrorHTML;

	$("#displayWindow").show();
	hidePreloader();
}

/*On Receiving Results from Server
 * Fetch the content and display to the user
 */

socket.on('hashtagResults',function(data){
	console.log("Positive count: "+data.positive_count);
	console.log("Negative count: "+data.negative_count);
	console.log("Neutral count: "+data.neutral_count);	

	displayTweets(data.positive_tweet_list,
				  data.negative_tweet_list,
				  data.neutral_tweet_list);

	displayChart(data.positive_count,
				 data.negative_count,
				 data.neutral_count);
});

/*On Receiving an Error Message from Server*/
socket.on('Error',function(data){
	console.log("Recv Error from server");
	displayErrorMessage(data.msg);
	socket.disconnect();
});
