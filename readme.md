---This project has been developed using Node.js---
-To run it, you will require node.js installed in your system.
-Following steps are mentioned to install node.js in your machine.(For Linux Users only)
-Sample Screenshots are present in the results folder

Steps 1-6 can be avoided by running the scripts:

To setup environment:

sudo bash setup.sh

To run the program:

sudo bash run.sh

1) First Step is to install node.js on your system. These instructions are specifically for a Linux Environment (Tested on Ubuntu 14.04 64bit).

	sudo apt-get update
	sudo apt-get install git-core curl build-essential openssl libssl-dev
	git clone https://github.com/joyent/node.git && cd node
	pwd
	./configure
	make
	sudo make install

2) Type node --version
If node.js has been successfully installed the version installed will be displayed.

3) Install Jade, using the following steps:
	sudo npm set registry http://registry.npmjs.org/
	sudo npm install -g jade
	sudo npm install -g bower


4) Once installed, go to the src folder in the submitted solution from the terminal.

	cd src

5) Type node app.js and Press Enter. This will make the node.js server run which will fetch the tweets. 

6) To view the results open your internet browser and go to the following address:

	localhost:3000
7) Enter any Term you would like to search on twitter and press the search button. Results will be shown after 10 seconds.
    


